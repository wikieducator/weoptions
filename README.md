# WEoptions extension for Mediawiki

## Purpose

This extension adds Mediawiki API access to the user options for
Mediawiki 1.15 to 1.19. Starting with Mediawiki 1.20 there is an
official API eliminating the need for this extension.

## Status

This software has been in use for over a year and is considered
stable.

## Installation

1. Copy this directory to `extensions/WEoptions` inside your
MediaWiki directory.
2. Add this line to the end of your LocalSettings.php:

    require_once("$IP/extensions/WEoptions/WEoptions.php');

## Contact

* Jim Tittsler
* jim@OERfoundation.org
* jimt on Gitorious
* jimt in #wikieducator on irc.freenode.net

