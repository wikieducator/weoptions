<?php
# ex: tabstop=8 shiftwidth=8 noexpandtab
/**
* @package MediaWiki
* @subpackage WEoptions
* @author Jim Tittsler <jim@OERfoundation.org>
* @licence GPL2
*/

define('DEBUG_WEOPTIONS', true);
define('DEBUG_WEOPTIONS_FILE', '/tmp/options.log');

if( !defined( 'MEDIAWIKI' ) ) {
	die( "This file is an extension to the MediaWiki software and cannot be used standalone.\n" );
}

$wgExtensionCredits['parserhook'][] = array(
	'path'           => __FILE__,
	'name'           => 'WEoptions',
	'version'        => '0.4.1',
	'url'            => 'http://WikiEducator.org/Extension:WEoptions',
	'author'         => '[http://WikiEducator.org/User:JimTittsler Jim Tittsler]',
        'description'    => 'add API calls for changing options for current user',
);

$wgAPIModules['weoptions'] = 'APIWEoptions';

class APIWEoptions extends ApiQueryBase {
	public function __construct( $query, $moduleName ) {
		parent :: __construct( $query, $moduleName, 'ch' );
	}

	function dlog($s) {
		if (DEBUG_WEOPTIONS) {
			error_log($s, 3, DEBUG_WEOPTIONS_FILE);
		}
	}

	public function execute() {
		global $wgUser, $wgServer;
		global $wgDefaultUserOptions;
		$id = NULL;
		$user = $wgUser->getId();
		$params = $this->extractRequestParams();

		if ( $user <= 0 ) {
			$this->dieUsage('Anonymous users cannot change preferences',
				'notloggedin');
		}
		if (!isset($params['ange'])) {
			$this->dieUsage('change argument not supplied',
				'nochanges');
		}
		$changes = explode('|', $params['ange']);
		$changecnt = 0;
		foreach ($changes as &$change) {
			$kv = explode('=', $change, 2);
			$this->dlog(print_r($kv, true));
			if (count($kv) <> 2) {
				$this->dieUsage('missing key=value',
					'angeinvalid');
			}
			$k = trim($kv[0]);
			$v = trim($kv[1]);
			if (!array_key_exists($k, $wgDefaultUserOptions)) {
				$this->dieUsage('unknown option key',
					'angebadoption');
			}
			$old = $wgUser->getOption($k);
			$this->dlog("Old value of $k=$old\n");
			if ($v <> $old) {
				$this->dlog("Set option $k=$v\n");
				$wgUser->setOption($k, trim($kv[1]));
				$changecnt++;
			}
		}
		if ($changecnt > 0) {
			$this->dlog("Saving $changecnt change(s)\n");
			$wgUser->saveSettings();
			$this->dlog(" saved\n");
		}
		#$vid = preg_replace('/[^-_.a-z0-9]/i', '', $params['change']);
		$result = $this->getResult();
		$result->addValue('options', $this->getModuleName(), true);
	}

	public function getAllowedParams() {
		return array (
			'ange' => null
		);
	}

	public function getParamDescription() {
		return array (
			'ange' => 'pipe-separated list of changes'
		);
	}

	public function getDescription() {
		return 'change user preferences';
	}

	protected function getExamples() {
		return array (
			'api.php?action=weoptions&change=skin=vector',
		);
	}

	public function getVersion() {
		return __CLASS__ . ': 0';
	}
}

